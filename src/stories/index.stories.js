import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import TodoItem from "../components/TodoItem/TodoItem.jsx";
import TodoList from "../components/TodoList/TodoList.jsx";

const item = {
    id: "1",
    text: "my item",
    finished: false,
};
const itemFinished = {
    id: "2",
    text: "finished item",
    finished: true,
};

const defaultsItem = {
    item: item,
    onEdit: action("onEdit"),
    onDelete: action("onDelete"),
};

storiesOf("TodoItem", module)
    .add("default", () => <TodoItem {...defaultsItem} />)
    .add("completed", () => <TodoItem {...defaultsItem} item={itemFinished} />);

const defaultsList = {
    items: [item, itemFinished],
    itemsUpdating: false,
    onRefresh: action("onRefresh"),
    onClearCompleted: action("onClearCompleted"),
    onAddItem: action("onAddItem"),
    onEditItem: action("onEditItem"),
    onDeleteItem: action("onDeleteItem"),
};

storiesOf("TodoList", module)
    .add("default", () => <TodoList {...defaultsList} />)
    .add("loading", () => <TodoList {...defaultsList} itemsUpdating={true} />);
