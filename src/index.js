// @flow
import * as React from "react";
import ReactDOM from "react-dom";
import Todo from "./components/Todo/Todo.jsx";
import InMemoryTodoApi from "./api/InMemoryApi";
import styles from "./style.less";

const memoryApi = new InMemoryTodoApi();
class App extends React.Component<{}, {}> {
    render(): React.Node {
        return (
            <div>
                <Todo api={memoryApi} />
            </div>
        );
    }
}

const root = document.getElementById("root");
if (root) {
    ReactDOM.render(<App />, root);
}
