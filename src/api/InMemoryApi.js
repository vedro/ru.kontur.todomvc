// @flow
function delay(timeout: number): Promise<void> {
    return new Promise(resolve => setTimeout(resolve, timeout));
}

const ERROR_RATE = 0.2;

export default class InMemoryTodoApi implements ITodoApi {
    items: Array<TodoItemType>;
    idIncrement: number;

    constructor() {
        this.items = [];
        this.idIncrement = 0;
    }

    randomRaise(msg: string) {
        if (Math.random() < ERROR_RATE) throw new Error(msg);
    }

    async addItem(text: string): Promise<TodoItemType> {
        if (!text) throw new Error("Empty item");
        await delay(1000);
        this.randomRaise("Adding failed");
        this.idIncrement++;
        const result = {
            id: this.idIncrement.toString(),
            text: text,
            finished: false,
        };
        this.items.push(result);
        return result;
    }

    async updateItem(id: string, item: TodoItemType): Promise<TodoItemType> {
        await delay(300);
        this.randomRaise("Updating failed");
        const updatedItem = {
            ...this.items.find(item => item.id === id),
            ...item,
            id: id,
        };
        return updatedItem;
    }

    async removeItem(id: string): Promise<void> {
        await delay(1000);
        this.randomRaise("Remove failed");
        this.items = this.items.filter(item => item.id !== id);
    }

    async clearCompleted(): Promise<void> {
        await delay(1000);
        this.randomRaise("Clearing failed");
        this.items = this.items.filter(item => !item.finished);
    }

    async getAllItems(): Promise<Array<TodoItemType>> {
        await delay(500);
        this.randomRaise("Get All failed. Try to reload Page");
        return [...this.items];
    }
}

// const api = new InMemoryTodoApi();
