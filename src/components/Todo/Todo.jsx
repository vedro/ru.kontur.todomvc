// @flow
import * as React from "react";
import TodoList from "../TodoList/TodoList.jsx";
import { Toast } from "../../ui";

type Props = {
    api: ITodoApi,
};

type State = {
    items: Array<TodoItemType>,
    itemsUpdating: boolean,
};

export default class Todo extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            items: [],
            itemsUpdating: false,
        };
    }

    componentDidMount() {
        this.loadItems();
    }

    handleRefresh = () => {
        this.loadItems();
    };

    showError(text: string) {
        Toast.push(text);
    }

    async doApiRequest(fn: () => Promise<void>): Promise<void> {
        this.setState({ itemsUpdating: true });
        try {
            await fn();
        } catch (e) {
            this.showError(e.message);
        } finally {
            this.setState({ itemsUpdating: false });
        }
    }

    loadItems = () => {
        this.doApiRequest(async () => {
            this.setState({ items: await this.props.api.getAllItems() });
        });
    };

    addItem = (text: string) => {
        this.doApiRequest(async () => {
            this.setState({
                items: [...this.state.items, await this.props.api.addItem(text)],
            });
        });
    };

    editItem = (index: number, updatedItem: TodoItemType) => {
        this.doApiRequest(async () => {
            const { items } = this.state;
            const savedItem = await this.props.api.updateItem(items[index].id, updatedItem);
            this.setState({
                items: [...items.slice(0, index), savedItem, ...items.slice(index + 1)],
            });
        });
    };

    deleteItem = (index: number) => {
        this.doApiRequest(async () => {
            const { items } = this.state;
            await this.props.api.removeItem(items[index].id);
            this.setState({
                items: [...items.slice(0, index), ...items.slice(index + 1)],
            });
        });
    };

    clearCompleted = () => {
        this.doApiRequest(async () => {
            await this.props.api.clearCompleted();
            this.setState({
                items: this.state.items.filter(item => !item.finished),
            });
        });
    };

    render(): React.Node {
        return (
            <div>
                <TodoList
                    items={this.state.items}
                    itemsUpdating={this.state.itemsUpdating}
                    onRefresh={this.handleRefresh}
                    onClearCompleted={this.clearCompleted}
                    onAddItem={this.addItem}
                    onEditItem={this.editItem}
                    onDeleteItem={this.deleteItem}
                />
            </div>
        );
    }
}
