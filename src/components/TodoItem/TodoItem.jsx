// @flow
import * as React from "react";
import { Checkbox, Input, Link } from "../../ui";
import styles from "./TodoItem.less";
import classNames from "classnames";

type Props = {
    item: TodoItemType,
    onEdit: (updatedItem: TodoItemType) => void,
    onDelete: () => void,
};

type State = {
    isEdit: boolean,
    value: string,
};

export default class TodoItem extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            isEdit: false,
            value: "",
        };
    }

    handleStartEdit() {
        this.setState({ isEdit: true, value: this.props.item.text });
    }

    handleFinishEdit(save: boolean) {
        if (save) {
            this.props.onEdit({ ...this.props.item, text: this.state.value });
        }
        this.setState({ isEdit: false, value: "" });
    }

    handleKeyUp(keyCode: number) {
        switch (keyCode) {
            case 13:
                this.handleFinishEdit(true);
                break;
            case 27:
                this.handleFinishEdit(false);
                break;
            default:
        }
    }

    handleComplete(isComplete: boolean) {
        this.props.onEdit({ ...this.props.item, finished: isComplete });
    }

    render(): React.Node {
        const { item, onDelete } = this.props;
        const { value, isEdit } = this.state;
        return (
            <div
                className={classNames({
                    [styles.todoItem]: true,
                    [styles.finished]: item.finished,
                })}>
                <div className={styles.checkboxWrap}>
                    <Checkbox checked={item.finished} onChange={(e, isComplete) => this.handleComplete(isComplete)} />
                </div>

                <div className={styles.title} onClick={() => this.handleStartEdit()}>
                    {isEdit ? (
                        <Input
                            value={value || ""}
                            onKeyUp={(e: SyntheticKeyboardEvent<>) => this.handleKeyUp(e.keyCode)}
                            onChange={(e, value) => this.setState({ value: value })}
                            onBlur={() => this.handleFinishEdit(false)}
                            width="100%"
                            autoFocus
                        />
                    ) : (
                        item.text
                    )}
                </div>

                <div className={styles.closer}>
                    <Link icon="Delete" tabIndex="-1" onClick={onDelete} />
                </div>
            </div>
        );
    }
}
