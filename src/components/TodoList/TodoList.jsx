// @flow
import * as React from "react";
import TodoItem from "../TodoItem/TodoItem.jsx";
import { Loader, Button, Group, Input, Link } from "../../ui";
import styles from "./TodoList.less";

type FiltersType = "All" | "Active" | "Finished";

const Filters: { [FiltersType]: FiltersType } = {
    All: "All",
    Active: "Active",
    Finished: "Finished",
};

type Props = {
    items: Array<TodoItemType>,
    itemsUpdating: boolean,
    onRefresh: () => void,
    onClearCompleted: () => void,
    onAddItem: (value: string) => void,
    onEditItem: (index: number, updatedItem: TodoItemType) => void,
    onDeleteItem: (index: number) => void,
};
type State = {
    filter: FiltersType,
    currentValue: string,
};

export default class TodoList extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            filter: "All",
            currentValue: "",
        };
    }

    handleKeyUp(keyCode: number) {
        const { currentValue } = this.state;
        const { itemsUpdating } = this.props;

        if (keyCode === 13 && !itemsUpdating) {
            this.props.onAddItem(currentValue);
            this.setState({ currentValue: "" });
        }
    }

    isItemMatchesCurrentFilter(item: TodoItemType): boolean {
        const { filter } = this.state;

        return (
            filter === Filters.All ||
            (filter === Filters.Active && !item.finished) ||
            (filter === Filters.Finished && item.finished)
        );
    }

    renderFooter(): React.Node {
        const hasFinished = this.props.items.some(item => item.finished);
        const unfinishedCount = this.props.items.filter(item => !item.finished).length;
        return (
            <div className={styles.footer}>
                <div className={styles.itemsLeft}>
                    {unfinishedCount} {unfinishedCount > 1 ? "items" : "item"} left
                </div>
                <div className={styles.filters}>
                    <Group>
                        {Object.keys(Filters).map(filter => (
                            <Button
                                key={filter}
                                checked={this.state.filter === filter}
                                onClick={() =>
                                    this.setState({
                                        filter: filter,
                                    })
                                }>
                                {filter}
                            </Button>
                        ))}
                    </Group>
                </div>
                <div className={styles.clear}>
                    {hasFinished && <Link onClick={this.props.onClearCompleted}>Clear Completed</Link>}
                </div>
            </div>
        );
    }
    render(): React.Node {
        return (
            <div className={styles.wrap}>
                <Link icon="Refresh" onClick={this.props.onRefresh}>
                    Reload
                </Link>
                <div className={styles.header}> todos </div>

                <Input
                    onChange={(e, value) => this.setState({ currentValue: value })}
                    onKeyUp={(e: SyntheticKeyboardEvent<>) => this.handleKeyUp(e.keyCode)}
                    value={this.state.currentValue || ""}
                    width="100%"
                    placeholder="What needs to be done?"
                    size="large"
                />

                <div className={styles.items}>
                    <Loader active={this.props.itemsUpdating} type="mini">
                        {this.props.items.map((item: TodoItemType, index) => {
                            if (!this.isItemMatchesCurrentFilter(item)) {
                                return null;
                            }
                            return (
                                <TodoItem
                                    key={item.id}
                                    item={item}
                                    onEdit={(updatedItem: TodoItemType) => this.props.onEditItem(index, updatedItem)}
                                    onDelete={() => this.props.onDeleteItem(index)}
                                />
                            );
                        })}
                    </Loader>
                </div>

                {this.props.items.length > 0 && this.renderFooter()}
            </div>
        );
    }
}
