const path = require("path");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const PROD = process.env.NODE_ENV === "production";

module.exports = {
    entry: {
        bundle: ["babel-polyfill", "./src/index.js"],
    },
    output: {
        filename: "[name].js",
        path: path.resolve(__dirname, "dist"),
    },
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: ["babel-loader", "ts-loader"],
            },

            PROD
                ? {
                      test: /\.less$/,
                      exclude: /node_modules/,
                      use: ExtractTextPlugin.extract({
                          fallback: "style-loader",
                          use: ["css-loader", "less-loader"],
                      }),
                  }
                : {
                      test: /\.less$/,
                      exclude: /node_modules/,
                      use: ["style-loader", "css-loader", "less-loader"],
                  },

            {
                test: /\.jsx?$/,
                include: /retail-ui/,
                use: ["babel-loader"],
            },
            {
                test: /\.less$/,
                include: /retail-ui/,
                use: ["style-loader", "css-loader", "less-loader"],
            },
            {
                test: /\.(woff|eot|png)/,
                include: /retail-ui/,
                use: ["file-loader"],
            },
        ],
    },
    plugins: PROD
        ? [
              new ExtractTextPlugin("styles.css"),
              new HtmlWebpackPlugin({
                  template: "./src/index.html",
              }),
          ]
        : [
              new HtmlWebpackPlugin({
                  template: "./src/index.html",
              }),
          ],
};
